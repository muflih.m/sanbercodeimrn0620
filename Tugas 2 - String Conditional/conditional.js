//If Else
console.log("Nomor 1:");
//variable
var nama = "Junaedi";		// assign nama dengan string
var peran = "Penyihir";		// assign peran dengan string Penyiir atau Guard ata Warewolf 

if (nama == null || nama == "") {
	console.log("Nama harus diisi!");
} else {
	if (peran == null || peran == "") {
		console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
	} else {
		console.log("Selamat datang di Dunia Werewolf, " + nama + "!");
		if (peran == "Penyihir") {
			console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
		} else if (peran == "Guard") {
			console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf!");
		} else if (peran == "Werewolf") {
			console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!");
		}
	}		
}
console.log("=================================================================")
console.log(" ")

//Switch Case
console.log("Nomor 2:");
//variable
var hari = 1;		// assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 12;		// assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2110;	// assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if (hari >= 1 && hari <= 31  ) {
	if (bulan >= 1 && bulan <= 12) {
		if (tahun >= 1900 && tahun <= 2200) {
			switch(bulan) {
	case 1:
		var namabulan = "Januari";
		break;
	case 2:
		var namabulan = "Februari";
		break;
	case 3:
		var namabulan = "Maret";
		break;
	case 4:
		var namabulan = "April";
		break;
	case 5:
		var namabulan = "Mei";
		break;
	case 6:
		var namabulan = "Juni";
		break;
	case 7:
		var namabulan = "Juli";
		break;
	case 8:
		var namabulan = "Agustus";
		break;
	case 9:
		var namabulan = "September";
		break;
	case 10:
		var namabulan = "Oktober";
		break;
	case 11:
		var namabulan = "November";
		break;
	case 12:
		var namabulan = "Desember";
		break;
	}
		console.log(hari + " " + namabulan + " " + tahun);
		} else {
			console.log("error");
		}
	} else {
		console.log("error");
	}
} else {
	console.log("error");
}
