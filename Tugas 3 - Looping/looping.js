//Nomor 1 Looping while
console.log("Looping while");
//variable
console.log("LOOPING PERTAMA");
var a = 0;
//algoritm
while (a < 20) {
	a = a + 2;
	console.log(a + " - I love coding");
}

console.log("LOOPING KEDUA");
var b = 22;
while (b > 2) {
	b = b - 2;
	console.log(b + " - I will become a mobile development");
}
console.log("=================================================================");
console.log(" ");

//Nomor 2 Looping menggunakan for
console.log("Looping for");
//algoritm
for (i = 1; i <= 20; i ++) {
	if (i%3 == 0 && i%2 != 0) {
		console.log(i + " - I Love Coding");
	}
	else if (i % 2 != 0) {
		console.log(i + " - Santai");
	}
	else if (i % 2 == 0) {
		console.log(i + " - Berkualitas");
	}
}
console.log("=================================================================");
console.log(" ");

//Nomor 3 Membuat Persegi Panjang #
console.log("Membuat persegi panjang #");
//algoritm
for (i = 1; i <= 4; i++){
	console.log("########");
}
console.log("=================================================================");
console.log(" ");

//Nomor 4 Membuat tangga 
console.log("Membuat tangga")
//algoritm
var tagar = "#"
for (i = 1; i <= 7; i++) {
	console.log(tagar);
	tagar = tagar + "#";
}
console.log("=================================================================");
console.log(" ");

//Nomor 5 Membuat papan catur
console.log("Membuat papan catur")
//agoritm
for (i = 1; i <= 8; i++) {
	if (i%2 != 0) {
		console.log(" # # # #");
	} else {
		console.log("# # # # ");
	}
}