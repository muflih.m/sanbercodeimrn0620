//Nomor 1
console.log("Nomor 1:");
//function
function teriak() {
	return "Halo Sanbers!"
}
console.log(teriak())
console.log("=================================================================")
console.log(" ")

//Nomor 2
console.log("Nomor 2:");
//function
function kalikan(x1, x2) {
	return x1 * x2
}
//variable
var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali)				//akan menampilkan angka 48 (bukan 30), yang mana merupakan hasil perkalian dari 12 dan 4
console.log("=================================================================")
console.log(" ")

//Nomor 3
console.log("Nomor 3:");
//function
function intorduce(nama, umur, alamat, hobi) {
	var kalimat = "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!";
	return kalimat
}
//variable
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = intorduce(name, age, address, hobby);
console.log(perkenalan)