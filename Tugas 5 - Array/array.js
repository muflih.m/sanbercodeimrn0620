//Nomor 1 Range
console.log("Nomor 1 Range:");
//function
function range(startNum, finishNum) {
	var A = [0];
	if(startNum < finishNum) {
		A.splice(0, 1, startNum)
		for(i = startNum + 1; i <= finishNum; i++) {
			A.push(i);
		}
	} else if(startNum > finishNum) {
		A.splice(0, 1, startNum)
		for(i = startNum - 1; i >= finishNum; i--) {
			A.push(i);
		}
	} else {
		A = -1;
	}
	return A
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log("=================================================================")
console.log(" ")

//Nomor 2 Range with Step
console.log("Nomor 2 Range with Step:")
//function
function rangeWithStep(startNum, finishNum, step) {
	var A = [0];
	if(startNum < finishNum) {
		A.splice(0, 1, startNum)
		var i = startNum;
		while(i <= (finishNum-step)) {
			i = i + step;
			A.push(i);
		}
	} else if(startNum > finishNum) {
		A.splice(0, 1, startNum)
		var i = startNum;
		while(i >= (finishNum+step)) {
			i = i - step;
			A.push(i);
		}
	} else {
		A = -1;
	}
	return A
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log("=================================================================")
console.log(" ")

//Nomor 3 Sum of Range
console.log("Nomor 3 Sum of Range")
//function
function sum(startNum, finishNum, step=1) {
	var sum = 0;
	if(startNum != null && finishNum != null) {
		var B = rangeWithStep(startNum, finishNum, step);
		for(i = 0; i <= B.length-1; i++) {
			sum = sum + B[i];	
		}
	} else if(startNum == null) {
		sum = 0;
	} else {
		sum = startNum;
	}
	return sum
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10 , 2))
console.log(sum(1))
console.log(sum())
console.log("=================================================================")
console.log(" ")

//Nomor 4 Array Multidimensi
console.log("Nomor 4 Array Multidiensi:")
//variable
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
//declare data adalah array 1x1
var dataNomor = [0]
var dataNama = [0]
var dataTempat = [0]
var dataTanggal = [0]
var dataHobi = [0]
//function
function dataHandling() {
	//untuk baca data
	//ubah data array
	dataNomor.splice(0, 1, input[0][0]);
	dataNama.splice(0, 1, input[0][1]);
	dataTempat.splice(0, 1, input[0][2]);
	dataTanggal.splice(0, 1, input[0][3]);
	dataHobi.splice(0, 1, input[0][4]);
	//input dari data input
	for(i = 1; i <= 3; i++) {
		dataNomor.push(input[i][0]);
		dataNama.push(input[i][1]);
		dataTempat.push(input[i][2]);
		dataTanggal.push(input[i][3]);
		dataHobi.push(input[i][4]);
	}
	//untuk keluarin dari input
	for(j = 0; j <= 3; j++) {
		var nomorID = "Nomor ID: 	" + dataNomor[j];
		var namaLengkap = "Nama Lengkap: 	" + dataNama[j];
		var TTL = "TTL: 	" + dataTempat[j] + ", " + dataTanggal[j];
		var hobi = "Hobi: 	" + dataHobi[j];
		console.log(nomorID)
		console.log(namaLengkap)
		console.log(TTL)
		console.log(hobi)
		console.log(" ")
	}
	return("")
}
console.log(dataHandling())
console.log("=================================================================")
console.log(" ")

//Nomor 5 Balik Kata
console.log("Nomor 5 Balik Kata:")
//function
function balikKata(kataLama) {
	var kataBaru = "";
	for(i = 0; i < kataLama.length+1; i++) {
		var huruf = kataLama.charAt(kataLama.length-i);
		kataBaru = kataBaru + huruf;
	}
	return kataBaru
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))
console.log("=================================================================")
console.log(" ")

//Nomor 6 Metode Array
console.log("Nomor 6 Metode Array:")
//variable
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
//function
function dataHandling2(input) {
	input.splice(1, 1, "Roman Alamsyah Elsharawy");
	input.splice(4, 1, "Pria", "SMA Internasional Metro");
	var tanggal = input[3];
	var nama = input[1];
	var tanggalSplit = tanggal.split("/");
	var bulan = Number(tanggalSplit[1]);
	switch (bulan) {
		case 1:
			var namabulan = "Januari";
			break;
		case 2:
			var namabulan = "Februari";
			break;
		case 3:
			var namabulan = "Maret";
			break;
		case 4:
			var namabulan = "April";
			break;
		case 5:
			var namabulan = "Mei";
			break;
		case 6:
			var namabulan = "Juni";
			break;
		case 7:
			var namabulan = "Juli";
			break;
		case 8:
			var namabulan = "Agustus";
			break;
		case 9:
			var namabulan = "September";
			break;
		case 10:
			var namabulan = "Oktober";
			break;
		case 11:
			var namabulan = "November";
			break;
		case 12:
			var namabulan = "Desember";
			break;
	}
	var tanggalRapi = tanggalSplit.join("-");
	var tanggalUrut = tanggalSplit.sort(function(value1, value2){
		return value2 - value1
	})
	var namaSlice = nama.toString().slice(0, 14);
	//output
	console.log(input)
	console.log(namabulan)
	console.log(tanggalUrut)
	console.log(tanggalRapi)
	console.log(namaSlice)

	return("")
}
console.log(dataHandling2(input))