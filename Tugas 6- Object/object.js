//Nomot 1
console.log("Nomor 1 Array to Object:")
//variable
var now = new Date();
var thisYear = now.getFullYear();
//function
function age(num) {
	if(num != null && num <= thisYear) {
		return thisYear - num
	} else {
		return "Invalid birth year"
	}
}
function arrayToObject(arr) {
	for(i = 0; i <= 1; i++) {
		var personObject = {
			firstName : arr[i][0],
			lastName : arr[i][1],
			gender : arr[i][2],
			age : age(arr[i][3])
		}
		console.log((i+1) + ". " + personObject.firstName + ": ")
		console.log(personObject)
	}
	return ""
}
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
console.log("=================================================================")
console.log(" ")

//Nomor 2
console.log("Nomor 2 Shopping Time")
//function
function beliBarang(money, num) {
	//variable
	var beli = [];
	var kembali;
	//code
	if(money >= 2475000) {
		beli.push("Sepatu Stacattu", "Baju Zoro", "Baju H&N", "Sweater Unikloh", "Casing Handphone");
		kembali = money - 2475000;
	} else if(money >= 975000 && money < 2475000) {
		beli.push("Baju Zoro", "Baju H&N", "Sweater Unikloh", "Casing Handphone");
		kembali = money - 975000;
	} else if(money >= 425000 && money < 975000) {
		beli.push("Baju H&N", "Sweater Unikloh", "Casing Handphone");
		kembali = money - 425000;
	} else if(money >= 225000 && money < 425000) {
		beli.push("Sweater Unikloh", "Casing Handphone");
		kembali = money - 225000;
	} else if(money >= 50000 && money < 225000) {
		beli.push("Casing Handphone");
		kembali = money - 50000;
	}
	if(num%2 != 0) {
		return beli
	} else {
		return kembali
	}
}
function shoppingTime(memberId, money) {
	if(memberId == null || (String(memberId)) < 4) {
		console.log("Mohon maaf, toko X hanya berlaku untuk  member saja")
	} else if(money < 50000) {
		console.log("Mohon maaf, uang tidak cukup")
	} else {
		var shoppingList = {
			memberId: memberId,
			money: money,
			listPuchased: beliBarang(money, 1),	
			changeMoney: beliBarang(money, 2)
		}
		console.log(shoppingList)
	}
	return ""
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("=================================================================")
console.log(" ")

//Nomor 3
console.log("Nomor 3 Naik Angkot:")
//function
function tukar(rute) {
	switch(rute) {
		case "A":
			var jarak = 1;
			break;
		case "B":
			var jarak = 2;
				break;
		case "C":
			var jarak = 3;
			break;
		case "D":
			var jarak = 4;
			break;
		case "E":
			var jarak = 5;
			break;
		case "F":
			var jarak = 6;
			break;
	}
	return jarak
}
function naikAngkot(arrPenumpang) {
	var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	for(i = 0; i < 2; i++) {
		var naik = arrPenumpang[i][1];
		var turun = arrPenumpang[i][2];
		var listPenumpang = {
			penumpang: arrPenumpang[i][0],
			naikDari: arrPenumpang[i][1],
			tujuan: arrPenumpang[i][2],
			bayar: 2000 * (tukar(turun) - tukar(naik))
		}
		console.log(listPenumpang)
	}
	return ""
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
