//Nomor 1
console.log('Nomor 1 mengubah fungsi menjadi fungsi arrow:')
//function
const golden = () => {console.log("this is golden!!")};
golden()
console.log("=================================================================")
console.log(" ")


//Nomor 2
console.log("Nomor 2 menyederhanakan menjadi object literal di ES6:")
const newFunction = (firstName, lastName) => {
	return {
		firstName,
		lastName,
		fullName() {
			console.log(`${firstName} ${lastName}`)
		}
	}
}
newFunction("William", "Imoh").fullName()
console.log("=================================================================")
console.log(" ")


//Nomor 3
console.log("Nomor 3 destructuring:")
const newObject = {
	firstName: "Harry",
	lastName: "Potter Holt",
	destination: "Hogwarts React Conf",
	occupation: "Dave-wizard Avocado",
	spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject;
//Driver code
console.log(firstName, lastName, destination, occupation)
console.log("=================================================================")
console.log(" ")


//Nomor 4
console.log("Nomor 4 array spreading:")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east];
console.log(combined)
console.log("=================================================================")
console.log(" ")


//Nomor 5
console.log("Nomor 5 template literals:")
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)